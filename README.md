# Perception Engine Road Occupancy Processor

This package generates the occupancy grid indicating the status of the road. It uses the point cloud from a filtered sensor and the ADAS Map data.

The occupancy grid can be conceptualized as a one dimensional 8-bit depth bitmap.

This package publishes a GridMap/OccupancyGrid with four different possible values:
- UNKNOWN
- NO ROAD
- OCCUPIED

These values can be set using parameters, as described in the *Configuration Parameters* section below.

### Extra requirements for compilation
- GridMap (http://wiki.ros.org/grid_map)

### Prerequisites

- Ground filtering

### Data subscription
This node subscribes to:
- Ground filtered in `/ground_points` (sensor_msgs::PointCloud)
- Obstacle points in `/object_points` (sensor_msgs::PointCloud)

### Data publishing
- `gridmap_road_status` publishes the native format of the grid_map package. This can be used to extend the functionalities of this package.
- `occupancy_road_status` publishes the native ros `nav_msgs::OccupancyGrid` message.

### How to run
From a sourced terminal in the workspace:
`roslaunch road_occupancy_processor road_occupancy_processor.launch`

### Quick Setup
**This is only a quick guide, each node must be properly configured**

1. Launch ray_ground_filter (Sensing tab/Points Preprocessor)
1. Launch road_occupancy_processor (Computing tab/Localization)

### Configuration parameters
* `points_ground_src` (default=`"points_ground"`) defines the PointCloud source topic containing only the ground points.
* `points_no_ground_src` (default=`"points_no_ground"`) defines the PointCloud source topic containing only the obstacle points.

### Coordinate Frame
The occupancy grid is published in the same coordinate frame as the input Point Cloud

### Color representation using default values

**Black** color represents areas not defined as NO ROAD in the vector map.

**Dark gray** indicates UNKNOWN areas as detected by this node.

**Light gray** means the area is FREE.

**White** is used for OCCUPIED areas.
