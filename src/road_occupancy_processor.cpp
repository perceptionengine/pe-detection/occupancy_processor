/*
 * Copyright 2020. Perception Engine/ All rights reserved.
 * Copyright 2018-2019 Autoware Foundation. All rights reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ********************
 *  v1.0: amc-nu (abrahammonrroy@yahoo.com)
 *  v2.0: amc-nu
 *
 * road_occupancy_processor.cpp
 */

#include "road_occupancy_processor/road_occupancy_processor.h"

void PeRoadOccupancyProcessorApp::ConvertXYZIToRTZ(const pcl::PointCloud<pcl::PointXYZI>::Ptr in_cloud,
                                                   PeRoadOccupancyProcessorApp::PointCloudXYZIRTColor &out_organized_points,
                                                   std::vector<pcl::PointIndices> &out_radial_divided_indices,
                                                   std::vector<PeRoadOccupancyProcessorApp::PointCloudXYZIRTColor> &out_radial_ordered_clouds)
{
  out_organized_points.resize(in_cloud->points.size());
  out_radial_divided_indices.clear();
  out_radial_divided_indices.resize(radial_dividers_num_);
  out_radial_ordered_clouds.resize(radial_dividers_num_);

  for (size_t i = 0; i < in_cloud->points.size(); i++)
  {
    PointXYZIRT new_point;
    auto radius = (float) sqrt(
      in_cloud->points[i].x * in_cloud->points[i].x
      + in_cloud->points[i].y * in_cloud->points[i].y
    );
    auto theta = (float) atan2(in_cloud->points[i].y, in_cloud->points[i].x) * 180 / M_PI;
    if (theta < 0)
    {
      theta += 360;
    }

    auto radial_div = (size_t) floor(theta / radial_divider_angle_);
    auto concentric_div = (size_t) floor(fabs(radius / concentric_divider_distance_));

    new_point.point = in_cloud->points[i];
    new_point.radius = radius;
    new_point.theta = theta;
    new_point.radial_div = radial_div;
    new_point.concentric_div = concentric_div;
    new_point.original_index = i;

    out_organized_points[i] = new_point;

    //radial divisions
    out_radial_divided_indices[radial_div].indices.push_back(i);

    out_radial_ordered_clouds[radial_div].push_back(new_point);

  }//end for

  //order radial points on each division
#pragma omp for
  for (size_t i = 0; i < radial_dividers_num_; i++)
  {
    std::sort(out_radial_ordered_clouds[i].begin(), out_radial_ordered_clouds[i].end(),
              [](const PointXYZIRT &a, const PointXYZIRT &b)
              {
                return a.radius < b.radius;
              });
  }
}

void PeRoadOccupancyProcessorApp::PublishGridMap(grid_map::GridMap &in_grid_map, const std::string &in_layer_publish)
{
  if (in_grid_map.exists(in_layer_publish))
  {
    grid_map_msgs::GridMap ros_gridmap_message;
    nav_msgs::OccupancyGrid ros_occupancygrid_message;
    grid_map::GridMapRosConverter::toMessage(in_grid_map, ros_gridmap_message);
    grid_map::GridMapRosConverter::toOccupancyGrid(in_grid_map,
                                                   in_layer_publish,
                                                   grid_min_value_,
                                                   grid_max_value_,
                                                   ros_occupancygrid_message);
    publisher_grid_map_.publish(ros_gridmap_message);
    publisher_occupancy_grid_.publish(ros_occupancygrid_message);
  } else
  {
    ROS_INFO("[%s] Empty GridMap. It might still be loading or it does not contain valid data.", __APP_NAME__);
  }
}

void PeRoadOccupancyProcessorApp::Convert3dPointToOccupancy(grid_map::GridMap &in_grid_map,
                                                            const geometry_msgs::Point &in_point,
                                                            cv::Point &out_point)
{
  // calculate position
  grid_map::Position map_pos = in_grid_map.getPosition();
  double origin_x_offset = in_grid_map.getLength().x() / 2.0 - map_pos.x();
  double origin_y_offset = in_grid_map.getLength().y() / 2.0 - map_pos.y();
  // coordinate conversion for cv image
  out_point.x = (in_grid_map.getLength().y() - origin_y_offset - in_point.y) / in_grid_map.getResolution();
  out_point.y = (in_grid_map.getLength().x() - origin_x_offset - in_point.x) / in_grid_map.getResolution();
}

void PeRoadOccupancyProcessorApp::DrawLineInGridMap(grid_map::GridMap &in_grid_map, cv::Mat &in_grid_image,
                                                    const geometry_msgs::Point &in_start_point,
                                                    const geometry_msgs::Point &in_end_point, uchar in_value)
{
  cv::Point cv_start_point, cv_end_point;
  Convert3dPointToOccupancy(in_grid_map, in_start_point, cv_start_point);
  Convert3dPointToOccupancy(in_grid_map, in_end_point, cv_end_point);

  cv::Rect rect(cv::Point(), in_grid_image.size());

  if (!rect.contains(cv_start_point) || !rect.contains(cv_end_point))
  {
    return;
  }

  const int line_width = 3;
  cv::line(in_grid_image, cv_start_point, cv_end_point, cv::Scalar(in_value), line_width);
}

void PeRoadOccupancyProcessorApp::SetPointInGridMap(grid_map::GridMap &in_grid_map, cv::Mat &in_grid_image,
                                                    const geometry_msgs::Point &in_point, uchar in_value)
{
  // calculate position
  cv::Point cv_point;
  Convert3dPointToOccupancy(in_grid_map, in_point, cv_point);

  cv::Rect rect(cv::Point(), in_grid_image.size());

  if (!rect.contains(cv_point))
    return;

  const int radius = 2;
  const int fill = -1;
  cv::circle(in_grid_image, cv::Point(cv_point.x, cv_point.y), radius, cv::Scalar(in_value), fill);
  //in_grid_image.at<u_char>(cv::Point(cv_point.x, cv_point.y)) = in_value;
}

pcl::PointCloud<pcl::PointXYZI>::Ptr  PeRoadOccupancyProcessorApp::GenerateRay(pcl::PointXYZI last_point,
  grid_map::GridMap& in_grid_map, cv::Mat &in_grid_image)
{
  pcl::PointCloud<pcl::PointXYZI>::Ptr ray_cloud_pcl(new pcl::PointCloud<pcl::PointXYZI>);
  double dist = sqrt(last_point.x*last_point.x + last_point.y*last_point.y);
  double precision = 0.001;
  double curr_t = 0;
  double curr_distance = 0;
  double max_distance = 70.0;
  bool keep_working = true;
  pcl::PointXYZI curr_point;
  do
  {
    curr_point.x = last_point.x * curr_t;
    curr_point.y = last_point.y * curr_t;
    curr_point.z = last_point.z * curr_t;
    //if (curr_point.z > 0.5 || curr_point.z < -0.5)
    {
      curr_point.z = 0.0;
    }
    curr_point.intensity = last_point.intensity;

    cv::Point cv_point;
    geometry_msgs::Point geo_curr_point;
    geo_curr_point.x = curr_point.x;
    geo_curr_point.y = curr_point.y;
    geo_curr_point.z = curr_point.z;
    Convert3dPointToOccupancy(in_grid_map, geo_curr_point, cv_point);

    cv::Rect rect(cv::Point(), in_grid_image.size());
    if (rect.contains(cv_point)
    && in_grid_image.at<uint8_t >(cv_point) == OCCUPANCY_ROAD_OCCUPIED)
    {
      keep_working = false;
    }

    //ray_cloud_pcl->points.push_back(curr_point);//add all points in ray
    curr_t += precision;
    curr_distance = sqrt(curr_point.x*curr_point.x + curr_point.y*curr_point.y);
  }while (curr_distance < max_distance && curr_distance < dist && keep_working);
  ray_cloud_pcl->points.push_back(curr_point);//add last point
  return ray_cloud_pcl;
}

void DrawLineStraightDown(cv::Mat in_image, int length, int x1, int y1, uchar in_color, uchar stop_at_color)
{
  for(int i = 0; i < length ; i++)
  {
    cv::Point point (x1, y1 + i );
    if (in_image.at<uchar>(point) == stop_at_color)
    {
      break;
    }
    in_image.at<uchar>(point) = in_color;
  }
}

void DrawLineStraightUp(cv::Mat in_image, int length, int x1, int y1, uchar in_color, uchar stop_at_color)
{
  for(int i = 0; i < length ; i++)
  {
    cv::Point point (x1, y1 - i);
    if (in_image.at<uchar>(point) == stop_at_color)
    {
      break;
    }
    in_image.at<uchar>(point) = in_color;
  }
}

void DrawLineStraightRight(cv::Mat in_image, int length, int x1, int y1, uchar in_color, uchar stop_at_color)
{
  for(int i = 0; i < length ; i++)
  {
    cv::Point point (x1 + i, y1);
    if (in_image.at<uchar>(point) == stop_at_color)
    {
      break;
    }
    in_image.at<uchar>(point) = in_color;
  }
}

void DrawLineStraightLeft(cv::Mat in_image, int length, int x1, int y1, uchar in_color, uchar stop_at_color)
{
  for(int i = 0; i < length ; i++)
  {
    cv::Point point (x1 - i, y1);
    if (in_image.at<uchar>(point) == stop_at_color)
    {
      break;
    }
    in_image.at<uchar>(point) = in_color;
  }
}

void DrawLineLeftUpward(cv::Mat in_image, int x1, int y1, int x2, int y2, uchar in_color, uchar stop_at_color)
{
  int dx = abs(x2 - x1);
  int dy = abs(y2 - y1);

  int y   = y1,
    eps = 0;

  for ( int x = x1; x >= x2; x-- )  {
    cv::Point point (x, y);
    if (in_image.at<uchar>(point) == stop_at_color)
    {
      break;
    }
    in_image.at<uchar>(point) = in_color;
    eps += dy;
    if ( (eps << 1) >= dx )  {
      y--;  eps -= dx;
    }
  }
}

void DrawLineLeftUDownward(cv::Mat in_image, int x1, int y1, int x2, int y2, uchar in_color, uchar stop_at_color)
{
  int dx = abs(x2 - x1);
  int dy = abs(y2 - y1);

  int y   = y1,
    eps = 0;

  for ( int x = x1; x >= x2; x-- )  {
    cv::Point point (x, y);
    if (in_image.at<uchar>(point) == stop_at_color)
    {
      break;
    }
    in_image.at<uchar>(point) = in_color;
    eps += dy;
    if ( (eps << 1) >= dx )  {
      y++;  eps -= dx;
    }
  }
}

void DrawLineRightUpward(cv::Mat in_image, int x1, int y1, int x2, int y2, uchar in_color, uchar stop_at_color)
{
  int dx = abs(x2 - x1);
  int dy = abs(y2 - y1);

  int y   = y1,
    eps = 0;

  for ( int x = x1; x <= x2; x++ )  {
    cv::Point point (x, y);
    if (in_image.at<uchar>(point) == stop_at_color)
    {
      break;
    }
    in_image.at<uchar>(point) = in_color;
    eps += dy;
    if ( (eps << 1) >= dx )  {
      y--;  eps -= dx;
    }
  }
}

void DrawLineRightUDownward(cv::Mat in_image, int x1, int y1, int x2, int y2, uchar in_color, uchar stop_at_color)
{
  int dx = abs(x2 - x1);
  int dy = abs(y2 - y1);

  int y   = y1,
    eps = 0;

  for ( int x = x1; x <= x2; x++ )  {
    cv::Point point (x, y);
    if (in_image.at<uchar>(point) == stop_at_color)
    {
      break;
    }
    in_image.at<uchar>(point) = in_color;
    eps += dy;
    if ( (eps << 1) >= dx )  {
      y++;  eps -= dx;
    }
  }
}

void DrawLineUpTo(cv::Mat in_image, float x1, float y1, float x2, float y2, uchar in_color, uchar stop_at_color)
{
  float dx = fabs(x2 - x1);
  float dy = fabs(y2 - y1);

  if ((y1 == y2) && (x1 == x2)) //same point
  {
    cv::Point point (x1,y1);
    in_image.at<uchar>(point) == stop_at_color;
  }
  if (x1 == x2)
  {
    if (y1 > y2) //going straight up
    {
      DrawLineStraightUp(in_image, (int)dy, (int)x1, (int)y1, in_color, stop_at_color);
    }
    if (y2 > y1) //going straight down
    {
      DrawLineStraightDown(in_image, (int)dy, (int)x1, (int)y1, in_color, stop_at_color);
    }
    return;
  }
  if(y1 == y2)
  {
    if (x1 > x2)
    {
      DrawLineStraightLeft(in_image, (int)dx, (int)x1, (int)y1, in_color, stop_at_color);
    }
    if (x2 > x1)
    {
      DrawLineStraightRight(in_image, (int)dx, (int)x1, (int)y1, in_color, stop_at_color);
    }
    return;
  }
  if (x1 > x2) //left
  {
    if (y1 > y2) // downward
    {
      DrawLineLeftUDownward(in_image, (int)x1, (int)y1, (int)x2, (int)y2, in_color, stop_at_color);
    }
    if(y2 > y1) // upward
    {
      DrawLineLeftUpward(in_image, (int)x1, (int)y1, (int)x2, (int)y2, in_color, stop_at_color);
    }
    return;
  }
  if (x1 < x2) //right
  {
    if (y1 > y2) //downward
    {
      DrawLineRightUDownward(in_image, (int)x1, (int)y1, (int)x2, (int)y2, in_color, stop_at_color);
    }
    if (y2 > y1) //upward
    {
      DrawLineRightUpward(in_image, (int)x1, (int)y1, (int)x2, (int)y2, in_color, stop_at_color);
    }
  }
}

void PeRoadOccupancyProcessorApp::PointsCallback(const sensor_msgs::PointCloud2::ConstPtr &in_ground_cloud_msg,
                                                 const sensor_msgs::PointCloud2::ConstPtr &in_no_ground_cloud_msg)
{

  grid_map::GridMap output_gridmap;
  output_gridmap.setFrameId(in_ground_cloud_msg->header.frame_id);
  output_gridmap.setTimestamp(in_ground_cloud_msg->header.stamp.toNSec());
  output_gridmap.setGeometry(grid_map::Length(input_gridmap_length_, input_gridmap_length_),
                             input_gridmap_resolution_,
                             grid_map::Position(input_gridmap_x_, input_gridmap_y_));

  pcl::PointCloud<pcl::PointXYZI>::Ptr final_ground_cloud(new pcl::PointCloud<pcl::PointXYZI>);
  pcl::PointCloud<pcl::PointXYZI>::Ptr final_no_ground_cloud(new pcl::PointCloud<pcl::PointXYZI>);
  pcl::PointCloud<pcl::PointXYZI>::Ptr last_noground_ray_cloud_pcl(new pcl::PointCloud<pcl::PointXYZI>);
  pcl::PointCloud<pcl::PointXYZI>::Ptr last_ground_ray_cloud_pcl(new pcl::PointCloud<pcl::PointXYZI>);

  pcl::fromROSMsg(*in_ground_cloud_msg, *final_ground_cloud);
  pcl::fromROSMsg(*in_no_ground_cloud_msg, *final_no_ground_cloud);

  std::string grid_layer_name = "empty";

  output_gridmap.add(grid_layer_name);
  output_gridmap[grid_layer_name].setConstant(OCCUPANCY_ROAD_UNKNOWN);

  cv::Mat original_image;
  grid_map::GridMapCvConverter::toImage<unsigned char, 1>(output_gridmap,
                                                          grid_layer_name,
                                                          CV_8UC1,
                                                          OCCUPANCY_ROAD_OCCUPIED,
                                                          OCCUPANCY_NO_ROAD,
                                                          original_image);

  PointCloudXYZIRTColor ground_organized_points;
  std::vector<pcl::PointIndices> ground_radial_division_indices;
  std::vector<PointCloudXYZIRTColor> ground_radial_ordered_clouds;

//  ConvertXYZIToRTZ(final_ground_cloud,
//                   ground_organized_points,
//                   ground_radial_division_indices,
//                   ground_radial_ordered_clouds);
//
//  for (size_t i = 0; i < ground_radial_ordered_clouds.size(); i++)
//  {
//    geometry_msgs::Point prev_point;
//    for (size_t j = 0; j < ground_radial_ordered_clouds[i].size(); j++)
//    {
//      geometry_msgs::Point current_point;
//      current_point.x = ground_radial_ordered_clouds[i][j].point.x;
//      current_point.y = ground_radial_ordered_clouds[i][j].point.y;
//      current_point.z = ground_radial_ordered_clouds[i][j].point.z;
//
//      DrawLineInGridMap(output_gridmap, original_image, prev_point, current_point, OCCUPANCY_ROAD_FREE);
//    }
//  }

  //process obstacle points
  for (const auto &point:final_no_ground_cloud->points)
  {
    geometry_msgs::Point sensor_point;
    sensor_point.x = point.x;
    sensor_point.y = point.y;
    sensor_point.z = point.z;
    SetPointInGridMap(output_gridmap, original_image, sensor_point, OCCUPANCY_ROAD_OCCUPIED);
  }
  geometry_msgs::Point origin_point_3d;
  cv::Point origin_point_2d;
  Convert3dPointToOccupancy(output_gridmap, origin_point_3d, origin_point_2d);
  for(size_t x = 0; x < original_image.cols; x++)
  {
    DrawLineUpTo(original_image, origin_point_2d.x, origin_point_2d.y, x, 0, OCCUPANCY_ROAD_FREE, OCCUPANCY_ROAD_OCCUPIED);
    DrawLineUpTo(original_image, origin_point_2d.x, origin_point_2d.y, x, original_image.rows, OCCUPANCY_ROAD_FREE, OCCUPANCY_ROAD_OCCUPIED);
  }
  for(size_t y = 0; y < original_image.rows; y++)
  {
    DrawLineUpTo(original_image, origin_point_2d.x, origin_point_2d.y, 0, y, OCCUPANCY_ROAD_FREE, OCCUPANCY_ROAD_OCCUPIED);
    DrawLineUpTo(original_image, origin_point_2d.x, origin_point_2d.y, original_image.cols, y, OCCUPANCY_ROAD_FREE, OCCUPANCY_ROAD_OCCUPIED);
  }
  cv::rotate(original_image, original_image, cv::ROTATE_90_CLOCKWISE);
  for(size_t x = 0; x < original_image.cols; x++)
  {
    DrawLineUpTo(original_image, origin_point_2d.x, origin_point_2d.y, x, 0, OCCUPANCY_ROAD_FREE, OCCUPANCY_ROAD_OCCUPIED);
    DrawLineUpTo(original_image, origin_point_2d.x, origin_point_2d.y, x, original_image.rows, OCCUPANCY_ROAD_FREE, OCCUPANCY_ROAD_OCCUPIED);
  }
  for(size_t y = 0; y < original_image.rows; y++)
  {
    DrawLineUpTo(original_image, origin_point_2d.x, origin_point_2d.y, 0, y, OCCUPANCY_ROAD_FREE, OCCUPANCY_ROAD_OCCUPIED);
    DrawLineUpTo(original_image, origin_point_2d.x, origin_point_2d.y, original_image.cols, y, OCCUPANCY_ROAD_FREE, OCCUPANCY_ROAD_OCCUPIED);
  }
  cv::rotate(original_image, original_image, cv::ROTATE_90_COUNTERCLOCKWISE);

  for (const auto &point:final_no_ground_cloud->points)
  {
    geometry_msgs::Point sensor_point;
    sensor_point.x = point.x;
    sensor_point.y = point.y;
    sensor_point.z = point.z;
    SetPointInGridMap(output_gridmap, original_image, sensor_point, OCCUPANCY_ROAD_OCCUPIED);
  }

  grid_map::GridMapCvConverter::addLayerFromImage<unsigned char, 1>(original_image,
                                                                    output_layer_name_,
                                                                    output_gridmap,
                                                                    OCCUPANCY_ROAD_OCCUPIED,
                                                                    OCCUPANCY_NO_ROAD);

  PublishGridMap(output_gridmap, output_layer_name_);
}

void PeRoadOccupancyProcessorApp::InitializeROSIo(ros::NodeHandle &in_private_handle)
{
  //get params
  std::string points_ground_topic_str, points_no_ground_topic_str, wayarea_topic_str;

  in_private_handle.param<std::string>("points_ground_src", points_ground_topic_str, "points_ground");
  ROS_INFO("[%s] points_ground_src: %s", __APP_NAME__, points_ground_topic_str.c_str());

  in_private_handle.param<std::string>("points_no_ground_src", points_no_ground_topic_str, "points_no_ground");
  ROS_INFO("[%s] points_no_ground_src: %s", __APP_NAME__, points_no_ground_topic_str.c_str());

  in_private_handle.param<std::string>("output_layer_name", output_layer_name_, "road_status");
  ROS_INFO("[%s] output_layer_name: %s", __APP_NAME__, output_layer_name_.c_str());

  in_private_handle.param<int>("road_unknown_value", OCCUPANCY_ROAD_UNKNOWN, 128);
  ROS_INFO("[%s] road_unknown_value: %d", __APP_NAME__, OCCUPANCY_ROAD_UNKNOWN);

  in_private_handle.param<int>("road_free_value", OCCUPANCY_ROAD_FREE, 75);
  ROS_INFO("[%s] road_free_value: %d", __APP_NAME__, OCCUPANCY_ROAD_FREE);

  in_private_handle.param<int>("road_occupied_value", OCCUPANCY_ROAD_OCCUPIED, 0);
  ROS_INFO("[%s] road_occupied_value: %d", __APP_NAME__, OCCUPANCY_ROAD_OCCUPIED);

  in_private_handle.param<int>("grid_length", input_gridmap_length_, 80);
  ROS_INFO("[%s] grid_length: %d", __APP_NAME__, OCCUPANCY_ROAD_OCCUPIED);
  in_private_handle.param<int>("grid_x", input_gridmap_x_, 0);
  ROS_INFO("[%s] grid_x: %d", __APP_NAME__, input_gridmap_x_);
  in_private_handle.param<int>("grid_y", input_gridmap_y_, 0);
  ROS_INFO("[%s] grid_y: %d", __APP_NAME__, input_gridmap_y_);
  in_private_handle.param<double>("grid_resolution", input_gridmap_resolution_, 0.2);
  ROS_INFO("[%s] grid_resolution: %.2f", __APP_NAME__, input_gridmap_resolution_);

  in_private_handle.param<double>("radial_divider_angle", radial_divider_angle_, 0.1);
  ROS_INFO("[%s] radial_divider_angle: %.2f", __APP_NAME__, radial_divider_angle_);

  if(radial_divider_angle_ == 0.0)
    radial_divider_angle_ = 0.5;

  radial_dividers_num_ = ceil(360 / radial_divider_angle_);

  cloud_ground_subscriber_ = new message_filters::Subscriber<sensor_msgs::PointCloud2>(node_handle_,
                                                                                       points_ground_topic_str, 1);
  ROS_INFO("[%s] Subscribing to... %s", __APP_NAME__, points_ground_topic_str.c_str());
  cloud_no_ground_subscriber_ = new message_filters::Subscriber<sensor_msgs::PointCloud2>(node_handle_,
                                                                                          points_no_ground_topic_str,
                                                                                          1);
  ROS_INFO("[%s] Subscribing to... %s", __APP_NAME__, points_no_ground_topic_str.c_str());

  cloud_synchronizer_ =
    new message_filters::Synchronizer<SyncPolicyT>(SyncPolicyT(10),
                                                   *cloud_ground_subscriber_,
                                                   *cloud_no_ground_subscriber_);
  cloud_synchronizer_->registerCallback(boost::bind(&PeRoadOccupancyProcessorApp::PointsCallback, this, _1, _2));

  //register publishers
  publisher_grid_map_ = node_handle_.advertise<grid_map_msgs::GridMap>("gridmap_road_status", 1);
  ROS_INFO("[%s] Publishing GridMap in gridmap_road_status", __APP_NAME__);

  publisher_occupancy_grid_ = node_handle_.advertise<nav_msgs::OccupancyGrid>("occupancy_road_status", 1);
  ROS_INFO("[%s] Publishing Occupancy grid in occupancy_road_status", __APP_NAME__);

  publisher_markers_ = node_handle_.advertise<visualization_msgs::MarkerArray>("road_obstacles", 1);
  ROS_INFO("[%s] Publishing Obstacles in road_obstacles", __APP_NAME__);

  publisher_cloud_ray_last_ = node_handle_.advertise<sensor_msgs::PointCloud2>("points_ray_last", 1);
  ROS_INFO("[%s] Publishing last points on each Ray in points_ray_last", __APP_NAME__);
}

void PeRoadOccupancyProcessorApp::Run()
{
  ros::NodeHandle private_node_handle("~");


  InitializeROSIo(private_node_handle);

  ROS_INFO("[%s] Ready. Waiting for data...", __APP_NAME__);

  ros::spin();

  ROS_INFO("[%s] END", __APP_NAME__);
}

PeRoadOccupancyProcessorApp::PeRoadOccupancyProcessorApp()
{

}
